# HAPROXY-KEEPALIVED-ANSIBLE

<img src="https://img.shields.io/static/v1?label=haproxy&message=v1.8.23&color=green&style=for-the-badge&logo=ghost"/>
<img src="https://img.shields.io/static/v1?label=keepalived&message=v2.0.10&color=green&style=for-the-badge&logo=ghost"/>
<img src="https://img.shields.io/static/v1?label=ansible&message=v2.9.6&color=green&style=for-the-badge&logo=Ansible"/>
<img src="https://img.shields.io/static/v1?label=licence&message=MIT&color=green&style=for-the-badge&logo=opensourceinitiative.svg"/>

**Conteúdo:**
• [Descrição do Projeto](#descrição-do-projeto) • [Pré-Requisitos](#pré-requisitos) • [Ambiente](#ambiente) • [Instalação](#instalação) • [Execução](#execução) • [Testes](#testes) • [Tecnologias](#tecnologias) • [Autor](#autor)

## Descrição do Projeto
<p align="center">Projeto para criação de 2 HAPROXY LoadBalancer para consultas LDAP em 2 ADs(Active Directory), com Ip flutuante(VRRP) utilizando o Keepalived e logs sendo enviados para Rsyslog. Deployment utilizando playbook Ansible via Bastion Host.</p>

## Pré-Requisitos

Antes de começar, você vai precisar ter instalado em seu Bastion Host as seguintes ferramentas:
[Git](https://git-scm.com), [Ansible](https://www.ansible.com/). 
Além disso é bom ter um editor para trabalhar como [Vim](https://www.vim.org/), [VSCode](https://code.visualstudio.com/) ou [Atom](https://atom.io/).

## Ambiente
| **Host**                      | **IP** |
|-------------------------------|--------|
| Bastion Host                  | x.x.x.10 |
| LB-master-node(haproxy01)     | x.x.x.1 |
| LB-backup-node(haproxy02)     | x.x.x.2 |
| Keepalived(VRRP)              | x.x.x.3 |
| DC-NODE-01                    | x.x.x.5 |
| DC-NODE-02                    | x.x.x.6 |

![](/images/haproxy-keepalived.png)

### Instalação

```bash

# Instale o Ansible
$ apt install ansible -y
ou
$ yum install ansible -y

#Instale o ldap-utils
$ apt install ldap-utils
ou
$ yum install ldap-utils


```

### Execução

```bash
# Acesse o Bastion Host
# Clone este repositório
$ git clone <https://gitlab.com/chavesbetto/haproxy-keepalived.git>

# Altere os endereços IPs nos arquivos conforme sua necessidade
$ vim keepalived/keepalived-master.conf
$ vim keepalived/keepalived-backup.conf

# Edite o arquivo haproxy.cfg com a sua preferência
$ vim haproxy/haproxy.cfg

# Troque chaves SSH no Bastion Host com os nodes que serão os HAPROXY.
$ ssh-keygen
$ ssh-copy-id -i  ~/.ssh/id_rsa.pub root@x.x.x.1
$ ssh-copy-id -i  ~/.ssh/id_rsa.pub root@x.x.x.2

#Edite o arquivo /etc/ansible/hosts no Bation Host com os Ips dos nodes HAPROXY
$ cat <<EOF > /etc/ansible/hosts
[lb-nodes]
x.x.x.1
x.x.x.2

[lb-master-node]
x.x.x.1

[lb-backup-node]
x.x.x.2
EOF

#Execute o playbook do ansible no Bastion Host
$ ansible-playbook -u root playbook.yaml

# Caso não tenha trocado chaves SSH utilize o comando
$ ansible-playbook --ask-pass -u root playbook.yaml

```

### Testes

```bash

# Teste de consulta Ldap

$ ldapsearch -LLL -H ldap://ldap.example.com -x -D "cn=search-user,ou=People,dc=example,dc=com" -b "dc=example,dc=com" -W -s sub 'sAMAccountName=test-user' 


```
Para pesquisa de usuário "test-user":

- -D - DistinguishedName do usuário que irá realizar a consulta, neste exemplo uso o usuário "search-user";
- -W - Password do usuário search-user;
- -H - URL do LDAP server ou IP, no nosso caso será o IP flutuante do keepalived (VRRP). Caso use SSL user "ldaps://";
- -b - Domínio da consulta;
- -s - Escopo da pesquisa - para pesquisar recursivamente na árvore (pode demorar um pouco)
- Finalmente, o filtro de pesquisa como um argumento não opcional. Neste caso, procuraremos o sAMAccountName of "test-user"

### Tecnologias

As seguintes ferramentas foram utilizadas para esse projeto:

- [Haproxy](https://www.haproxy.com/)
- [Keepalived](https://www.redhat.com/sysadmin/keepalived-basics)
- [Ansible](https://www.ansible.com/)

### Autor
---

 <img styl543109e="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/42034596?s=400&u=daa6bb8af8dfe08f055f1bea7caf4baae658e1e2&v=4" width="100px;" alt=""/>
 <br />
 <b>Beto Chaves</b>


Entre em contato!!

[![Linkedin Badge](https://img.shields.io/badge/-Beto-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/chavesbetto/)](https://www.linkedin.com/in/chavesbetto/) 
[![Gmail Badge](https://img.shields.io/badge/-chavesbetto@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:chavesbetto@gmail.com)](mailto:chavesbetto@gmail.com)
